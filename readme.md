Organism
========

**Organism** est un moteur permettant l'affichage de ses données et l'execution du code.

Chaque page du réseau est un code qui s'execute sur l'ensemble des messages.

Le code est choisi ou programmé par l'utilisateur en programmation visuelle et fonctionnelle (graphe d'instruction).
Le code peut décider si un message fait ou non partie de la page, si il est affichable et comment il est affiché.

Les messages ne contiennent pas de code, mais peuvent contenir un 'tag' permettant a un certain code de s'utiliser.
